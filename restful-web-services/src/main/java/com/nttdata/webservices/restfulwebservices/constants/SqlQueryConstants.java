package com.nttdata.webservices.restfulwebservices.constants;

import org.springframework.stereotype.Component;

@Component
public class SqlQueryConstants {

	public final String INSERT_VALUES = "INSERT INTO library_db (id, title, author, genre) VALUES(?,?,?,?)";
	public final String RETRIVE_ALL = "SELECT * FROM library_db";
	public final String RETRIVE_ALL_BY_AUTHOR = "SELECT * FROM library_db WHERE Author LIKE ?";
	public final String RETRIVE_ALL_BY_GENRE = "SELECT * FROM library_db WHERE Genre Like ?";
	public final String RETRIVE_ALL_BY_TITLE = "SELECT * FROM library_db WHERE Title Like ?";
	public final String RETRIVE_SINGLE = "SELECT * FROM library_db WHERE ID = ?";
	public final String DELETE_SINGLE = "DELETE FROM library_db WHERE ID = ?";
	public final String UPDATE_BOOK = "UPDATE library_db SET Title = ?, Author = ?, Genre = ? WHERE ID = ?";
	public final String UPDATE_BOOK_GENRE = "UPDATE library_db SET 'Genre' = ? WHERE ID = ?";
	public final String UPDATE_BOOK_AUTHOR = "UPDATE library_db SET 'Author' = ? WHERE ID = ?";

}
