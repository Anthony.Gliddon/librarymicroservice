package com.nttdata.webservices.restfulwebservices.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.nttdata.webservices.restfulwebservices.model.Book;


@Component
public class BookRepository {

	
	private static List<Book>library = new ArrayList<>();

	public List<Book> getLibrary() {
		library.add(new Book(1,"Hello World","Technology","A.Dent"));
		library.add(new Book(2,"Hello Java","Technology","B.Dent"));
		library.add(new Book(3,"Goodbye World","Technology","C.Dent"));
		return library;
	}



}
