package com.nttdata.webservices.restfulwebservices.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.nttdata.webservices.restfulwebservices.constants.ApplicationConstants;
import com.nttdata.webservices.restfulwebservices.interfaces.LibraryDao;
import com.nttdata.webservices.restfulwebservices.interfaces.LibraryServices;
import com.nttdata.webservices.restfulwebservices.model.Book;
import com.nttdata.webservices.restfulwebservices.model.CustomResponse;

@Service
@Component
public class BookServices implements LibraryServices {

	@Autowired
	private LibraryDao dao;
	private CustomResponse res;

	public BookServices(LibraryDao dao, CustomResponse res) {
		super();
		this.dao = dao;
		this.res = res;
	}

	/**
	 * @author GLIDDA
	 * @param Book
	 * @returns Custom Response
	 * 
	 * method for comlpetling add to Library actions on the called book. 
	 * returns a custom response of 200 if successfull or 400 if failed
	 *
	 */
	@Override
	public CustomResponse addBookToLibrary(Book newBook) {
		boolean isAdded = dao.addBookToLibrary(newBook);
		if (isAdded) {
			res.setResponseCode(ApplicationConstants.RESPONSE_CODE_OK);
			res.setResponseMsg(ApplicationConstants.RESPONSE_MESSAGE_OK);
		} else {
			res.setResponseCode(ApplicationConstants.RESPONSE_CODE_FAIL);
			res.setResponseMsg(ApplicationConstants.RESPONSE_MESSAGE_FAIL);
		}
		return res;
	}
	
	
	/**
	 * @author GLIDDA
	 * @param Book
	 * @returns Custom Response
	 * 
	 * method for completling search by ID actions on the called book.
	 * Checks if book is Null...
	 * returns a custom response of Success 200 if !null, or failed 400 if null.
	 *
	 */
	@Override
	public CustomResponse searchById(Long id) {

		Book book = dao.searchById(id);
		if (book != null) {
			res.setBook(book);
			res.setResponseCode(ApplicationConstants.RESPONSE_CODE_OK);
			res.setResponseMsg(ApplicationConstants.RESPONSE_MESSAGE_OK);
		} else {
			res.setResponseCode(ApplicationConstants.RESPONSE_CODE_FAIL);
			res.setResponseMsg(ApplicationConstants.RESPONSE_MESSAGE_FAIL);
		}

		return res;
	}
	
	
	/**
	 * @author GLIDDA
	 * @param Book
	 * @returns Custom Response including List<Book>
	 * 
	 * method for completling retireve all actions on the called dao layer. 
	 * returns a custom response of 200 if successfull or 400 if failed
	 *
	 */
	@Override
	public CustomResponse retrieveAllBooks() {
		List<Book> library = dao.retrieveAllBooks();

		if (!library.isEmpty()) {
			res.setLibrary(library);
			res.setResponseCode(ApplicationConstants.RESPONSE_CODE_OK);
			res.setResponseMsg(ApplicationConstants.RESPONSE_MESSAGE_OK);
		} else {
			res.setResponseCode(ApplicationConstants.RESPONSE_CODE_FAIL);
			res.setResponseMsg(ApplicationConstants.RESPONSE_MESSAGE_FAIL);
		}

		return res;
	}

	/**
	 * @author GLIDDA
	 * @param Book
	 * @returns Custom Response including List<Book>
	 * 
	 * method for completling search by Author actions on the called book. 
	 * returns a custom response of 200 if successfull or 400 if failed
	 *
	 *
	 *
	 */
	@Override
	public CustomResponse searchAllByAuthor(String author) {
		List<Book> library = dao.searchAllByAuthor(author);
		if (!library.isEmpty()) {
			res.setLibrary(library);
			res.setResponseCode(ApplicationConstants.RESPONSE_CODE_OK);
			res.setResponseMsg(ApplicationConstants.RESPONSE_MESSAGE_OK);
		} else {
			res.setResponseCode(ApplicationConstants.RESPONSE_CODE_FAIL);
			res.setResponseMsg(ApplicationConstants.RESPONSE_MESSAGE_FAIL);
		}

		return res;
	}
	
	
	/**
	 * @author GLIDDA
	 * @param Book
	 * @returns Custom Response  including List<Book>
	 * 
	 * method for completling search by ID actions on the called book. 
	 * returns a custom response of 200 if successfull or 400 if failed
	 *
	 */
	@Override
	public CustomResponse searchAllByGenre(String genre) {
		List<Book> library = dao.searchAllByGenre(genre);
		if (!library.isEmpty()) {
			res.setLibrary(library);
			res.setResponseCode(ApplicationConstants.RESPONSE_CODE_OK);
			res.setResponseMsg(ApplicationConstants.RESPONSE_MESSAGE_OK);
		} else {
			res.setResponseCode(ApplicationConstants.RESPONSE_CODE_FAIL);
			res.setResponseMsg(ApplicationConstants.RESPONSE_MESSAGE_FAIL);
		}

		return res;
	}
	
	
	
	/**
	 * @author GLIDDA
	 * @param Book
	 * @returns Custom Response including List<Book>
	 * 
	 * method for completling search by title actions on the called book. 
	 * returns a custom response of 200 if successfull or 400 if failed
	 *
	 */
	@Override
	public CustomResponse searchAllByTitle(String title) {
		List<Book> library = dao.searchAllByTitle(title);
		if (!library.isEmpty()) {
			res.setLibrary(library);
			res.setResponseCode(ApplicationConstants.RESPONSE_CODE_OK);
			res.setResponseMsg(ApplicationConstants.RESPONSE_MESSAGE_OK);
		} else {
			res.setResponseCode(ApplicationConstants.RESPONSE_CODE_FAIL);
			res.setResponseMsg(ApplicationConstants.RESPONSE_MESSAGE_FAIL);
		}

		return res;
	}

	
	/**
	 * @author GLIDDA
	 * @param Book
	 * @returns Custom Response
	 * 
	 * method for comlpetling update in Library actions on the called book. 
	 * returns a custom response of 200 if successfull or 400 if failed
	 *
	 */
	@Override
	public CustomResponse updateBook(Book book) {
		boolean isUpdated = dao.updateBook(book);
		
		if	(isUpdated=true) {
			res.setBook(book);
			res.setResponseCode(ApplicationConstants.RESPONSE_CODE_OK);
			res.setResponseMsg(ApplicationConstants.RESPONSE_MESSAGE_OK);
		} else {
			res.setResponseCode(ApplicationConstants.RESPONSE_CODE_FAIL);
			res.setResponseMsg(ApplicationConstants.RESPONSE_MESSAGE_FAIL);
		}
		return res;
	}

	/**
	 * @author GLIDDA
	 * @param Book
	 * @returns Custom Response
	 * 
	 * method for comlpetling Delete from Library actions on the called book. 
	 * returns a custom response of 200 if successfull or 400 if failed
	 *
	 */
	@Override
	public CustomResponse RemoveFromLibrary(long id) {
		if (dao.RemoveFromLibrary(id)) {
			res.setResponseCode(ApplicationConstants.RESPONSE_CODE_OK);
			res.setResponseMsg(ApplicationConstants.RESPONSE_MESSAGE_OK);
		} else {
			res.setResponseCode(ApplicationConstants.RESPONSE_CODE_FAIL);
			res.setResponseMsg(ApplicationConstants.RESPONSE_MESSAGE_FAIL);
		}
		return res;
	}

}
