package com.nttdata.webservices.restfulwebservices.util;

import java.util.List;
import java.util.stream.Collectors;

import com.nttdata.webservices.restfulwebservices.interfaces.Utilities;
import com.nttdata.webservices.restfulwebservices.model.Book;

public class SearchUtil implements Utilities{
	
	
/**	
 * @author GLIDDA
 * @param title
 * @param library
 * @return List<Book>
 * 
 * Method for Sorting through a List collection and returning values byt their title
 */
	public static List<Book> serachLibraryByTitle(String title, List<Book> library) {
		List<Book> librarySorted = library.stream().filter(book -> book.getTitle().contains(title)).collect(Collectors.toList());

		return librarySorted;
		
	}
	/**	
	 * @author GLIDDA
	 * @param author
	 * @param library
	 * @return List<Book>
	 * 
	 * Method for Sorting through a List collection and returning values byt their Author
	 */
	public static List<Book> serachLibraryByAuthor(String author, List<Book> library) {
		List<Book> librarySorted = library.stream().filter(book -> book.getAuthor().contains(author)).collect(Collectors.toList());

		return librarySorted;
		
	}
	/**	
	 * @author GLIDDA
	 * @param author
	 * @param library
	 * @return List<Book>
	 * 
	 * 
	 * Method for Sorting through a List collection and returning values byt their Genre
	 */
	public static List<Book> serachLibraryByGenre(String genre, List<Book> library) {
		List<Book> librarySorted = library.stream().filter(book -> book.getGenre().contains(genre)).collect(Collectors.toList());

		return librarySorted;
		
	}
	/**	
	 * @author GLIDDA
	 * @param author
	 * @param library
	 * @return Book
	 * 
	 * 
	 * Method for Sorting through a List collection and returning values byt their ID number
	 */
	public static Book searchById(long id, List<Book> library) {
		Book retBook = null;
		for (Book book : library) {

			if (id == book.getId()) {
				retBook = book;
			} else {
				System.out.println("no Book Found");
			}

		}
		return retBook;
	}
		
}
