package com.nttdata.webservices.restfulwebservices.util;

import java.util.Collections;
import java.util.List;

import com.nttdata.webservices.restfulwebservices.interfaces.Utilities;
import com.nttdata.webservices.restfulwebservices.model.Book;

public class ListSortUtil implements Utilities{

	
	public static List<Book> sortByIdAssending(List<Book> sortable){
		Collections.sort(sortable);
		List<Book>sorted = sortable;
		return sorted;		
	}
	
	public static List<Book> sortByIdDesending(List<Book> sortable){
		Collections.sort(sortable);
		Collections.reverse(sortable);
		List<Book>sorted = sortable;
		return sorted;	
	}
	
	public static List<Book> sortByNameAssending(List<Book> sortable){
		Collections.sort(sortable, new StringComparitor());
		List<Book>sorted = sortable;
		return sorted;
	}
	
	public static List<Book> sortByNameDesending(List<Book> sortable){
		Collections.sort(sortable, new StringComparitor());
		Collections.reverse(sortable);
		List<Book>sorted = sortable;
		return sorted;
	}
	
}
