package com.nttdata.webservices.restfulwebservices.model;

import java.io.Serializable;



public class Book implements Serializable, Comparable<Book> {
	
	
	private static final long serialVersionUID = 2643688818282961005L;
	private long id;
	private String title;
	private String genre;
	private String author;

	public Book() {
	}

	public Book(long id, String title, String genre, String author) {
		super();
		this.id = id;
		this.title = title;
		this.genre = genre;
		this.author = author;
	}
	

	public Book(String title, String genre, String author) {
		super();
		this.title = title;
		this.genre = genre;
		this.author = author;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", title=" + title + ", genre=" + genre + ", author=" + author + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((genre == null) ? 0 : genre.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (genre == null) {
			if (other.genre != null)
				return false;
		} else if (!genre.equals(other.genre))
			return false;
		if (id != other.id)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public int compareTo(Book book) {
		if(this.getId()>book.getId()) {
			return 1;
		}else if (this.getId()<book.getId()) {
			return -1;
		}
		return 0;
	}
}
