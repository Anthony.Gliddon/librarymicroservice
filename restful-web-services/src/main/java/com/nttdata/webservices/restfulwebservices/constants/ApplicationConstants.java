package com.nttdata.webservices.restfulwebservices.constants;

public class ApplicationConstants {
	
	public static final String RESPONSE_MESSAGE_OK="OK";
	public static final String RESPONSE_MESSAGE_FAIL="FAILED";
	
	public static final int RESPONSE_CODE_OK = 200;
	public static final int RESPONSE_CODE_FAIL = 400;
	

}
