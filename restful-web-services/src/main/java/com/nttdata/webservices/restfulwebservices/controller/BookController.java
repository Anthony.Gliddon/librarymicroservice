package com.nttdata.webservices.restfulwebservices.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nttdata.webservices.restfulwebservices.interfaces.LibraryServices;
import com.nttdata.webservices.restfulwebservices.model.Book;

@RestController
@RequestMapping("/books")
public class BookController {

	@Autowired
	private LibraryServices libServe;

	public BookController(LibraryServices libServe) {
		super();
		this.libServe = libServe;
	}

	/**
	 * @author GLIDDA
	 * @param id
	 * @return ResponseEntity<Book>
	 * 
	 * 
	 * Controller Method for retrieving a boik by  the id number
	 */
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Book> getBookById(@PathVariable Long id) {
		Book book = libServe.searchById(id).getBook();
		if (libServe.searchById(id).getResponseCode() == 200) {
			return new ResponseEntity<Book>(book, HttpStatus.FOUND);
		} else
			return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
	}

	/**
	 * @author GLIDDA
	 * @param author
	 * @return ResponseEntity<List<Book>>
	 * 
	 * controller Method for retrieving all books from the database by Author
	 */
	@GetMapping(path = "/by/{author}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Book>> getAllBooksByAuthor(@PathVariable("author") String author) {
		List<Book> library = libServe.searchAllByAuthor(author).getLibrary();
		if (libServe.searchAllByAuthor(author).getResponseCode() == 200) {
			return new ResponseEntity<List<Book>>(library, HttpStatus.FOUND);
		} else
			return new ResponseEntity<List<Book>>(HttpStatus.NOT_FOUND);
	}

	/**
	 * @author GLIDDA
	 * @param genre
	 * @return ResponseEntity<List<Book>>
	 * 
	 * controller Method for retrieving all books from the database by Genre
	 */
	@GetMapping(path = "/type/{genre}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Book>> getAllBooksByGenre(@PathVariable("genre") String genre) {
		List<Book> library = libServe.searchAllByGenre(genre).getLibrary();
		if (library != null) {
			return new ResponseEntity<List<Book>>(library, HttpStatus.FOUND);
		} else
			return new ResponseEntity<List<Book>>(HttpStatus.NOT_FOUND);
	}
	/**
	 * @author GLIDDA
	 * @param genre
	 * @return ResponseEntity<List<Book>>
	 * 
	 * controller Method for retrieving all books from the database by Title
	 */
	@GetMapping(path = "/called/{title}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Book>> getAllBooksByTitle(@PathVariable("title") String title) {
		List<Book> library = libServe.searchAllByTitle(title).getLibrary();
		if (libServe.searchAllByTitle(title).getResponseCode() == 200) {
			return new ResponseEntity<List<Book>>(library, HttpStatus.FOUND);
		} else
			return new ResponseEntity<List<Book>>(HttpStatus.NOT_FOUND);
	}
	/**
	 * @author GLIDDA
	 * @param genre
	 * @return ResponseEntity<List<Book>>
	 * 
	 * controller Method for retrieving all books from the database
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Book>> getAllBooks() {
		List<Book> library = libServe.retrieveAllBooks().getLibrary();
		if (libServe.retrieveAllBooks().getResponseCode() == 200) {
			return new ResponseEntity<List<Book>>(library, HttpStatus.FOUND);
		} else
			return new ResponseEntity<List<Book>>(HttpStatus.NOT_FOUND);
	}
	/**
	 * @author GLIDDA
	 * @param genre
	 * @return ResponseEntity<List<Book>>
	 * 
	 * controller Method for adding a book to the database
	 */
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Book> addBook(@RequestBody Book book) {

		if (libServe.addBookToLibrary(book).getResponseCode() == 200) {
			return new ResponseEntity<Book>(HttpStatus.CREATED);
		} else
			return new ResponseEntity<Book>(HttpStatus.NO_CONTENT);
	}
	/**
	 * @author GLIDDA
	 * @param genre
	 * @return ResponseEntity<List<Book>>
	 * 
	 * controller Method for updating a book in the database
	 */
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Book> updateBook(@RequestBody Book book) {
		
		if (libServe.updateBook(book).getResponseCode() == 200) {
			return new ResponseEntity<Book>(HttpStatus.OK);
		} else
			return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
	}
	
	/**
	 * @author GLIDDA
	 * @param genre
	 * @return ResponseEntity<List<Book>>
	 * 
	 * controller Method for Deleting a book from the database by id
	 */
	@DeleteMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Book> deleteBook(@PathVariable("id") Long id) {

		if (libServe.RemoveFromLibrary(id).getResponseCode() == 200) {
			return new ResponseEntity<Book>(HttpStatus.OK);
		} else
			return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
	}

}
