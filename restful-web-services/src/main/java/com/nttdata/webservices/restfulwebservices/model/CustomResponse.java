package com.nttdata.webservices.restfulwebservices.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class CustomResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private int responseCode;
	private String responseMsg;
	private List<Book> library;
	private Book book;

	public CustomResponse() {
	}

	public CustomResponse(int responseCode, String responseMsg, Book book) {
		super();
		this.responseCode = responseCode;
		this.responseMsg = responseMsg;
		this.book = book;
	}

	public CustomResponse(int responseCode, String responseMsg, List<Book> library) {
		super();
		this.responseCode = responseCode;
		this.responseMsg = responseMsg;
		this.library = library;
	}

	public CustomResponse(int responseCode, String responseMsg) {
		super();
		this.responseCode = responseCode;
		this.responseMsg = responseMsg;
	}

	public CustomResponse(int responseCode, String responseMsg, List<Book> library, Book book) {
		super();
		this.responseCode = responseCode;
		this.responseMsg = responseMsg;
		this.library = library;
		this.book = book;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

	public List<Book> getLibrary() {
		return library;
	}

	public void setLibrary(List<Book> library) {
		this.library = library;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((book == null) ? 0 : book.hashCode());
		result = prime * result + ((library == null) ? 0 : library.hashCode());
		result = prime * result + responseCode;
		result = prime * result + ((responseMsg == null) ? 0 : responseMsg.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomResponse other = (CustomResponse) obj;
		if (book == null) {
			if (other.book != null)
				return false;
		} else if (!book.equals(other.book))
			return false;
		if (library == null) {
			if (other.library != null)
				return false;
		} else if (!library.equals(other.library))
			return false;
		if (responseCode != other.responseCode)
			return false;
		if (responseMsg == null) {
			if (other.responseMsg != null)
				return false;
		} else if (!responseMsg.equals(other.responseMsg))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CustomResponse [responseCode=" + responseCode + ", responseMsg=" + responseMsg + ", library=" + library
				+ ", book=" + book + "]";
	}

}
