package com.nttdata.webservices.restfulwebservices.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.nttdata.webservices.restfulwebservices.constants.SqlQueryConstants;
import com.nttdata.webservices.restfulwebservices.exceptions.BookNotFoundException;
import com.nttdata.webservices.restfulwebservices.interfaces.LibraryDao;
import com.nttdata.webservices.restfulwebservices.model.Book;

/**
 * 
 * @author GLIDDA
 *
 */


@Repository
public class LibraryDaoImplJDBC implements LibraryDao {

	@Autowired
	JdbcTemplate jdbcTemplate;
	SqlQueryConstants sql;

	public LibraryDaoImplJDBC(JdbcTemplate jdbcTemplate, SqlQueryConstants sql) {
		super();
		this.jdbcTemplate = jdbcTemplate;
		this.sql = sql;
	}
	
	
	// TODO: Move all exception Handling from this layer up to the control layer to intercept issues 
	// earlier in the chain
	
	/**
	 * @author GLIDDA
	 * @param Book
	 * @returns Boolean
	 * 
	 * DAO method for completling add to Library actions on the called book. 
	 * returns a boolean 
	 *
	 */
	@Override
	public boolean addBookToLibrary(Book book) {

		int savedValue = 0;
		boolean isSaved = false;
		try {
			savedValue = jdbcTemplate.update(sql.INSERT_VALUES, book.getId(), book.getTitle(), book.getAuthor(),
					book.getGenre());
			if (savedValue == 1) {
				isSaved = true;
			} else {
				throw new BookNotFoundException();
			}
		} catch (BookNotFoundException e) {
			e.printStackTrace();
		}
		return isSaved;
	}
	
	/**
	 * @author GLIDDA
	 * @param Long id
	 * @returns Book
	 * 
	 * DAO method for completling add to Library actions on the called book. 
	 * returns a boolean 
	 *
	 */
	@Override
	public Book searchById(Long id) {
		Book newBook = null;
		try {
			Optional<Book> b1 = jdbcTemplate.queryForObject(sql.RETRIVE_SINGLE, new Object[] { id },
					(rs, rowNum) -> Optional.of(new Book(rs.getLong("id"), rs.getString("title"),
							rs.getString("author"), rs.getString("genre"))));

			if (b1.isPresent()) {
				return b1.get();
			} else {
				throw new BookNotFoundException();
			}
		} catch (BookNotFoundException e) {

			e.printStackTrace();
		}
		return newBook;
	}

	@Override
	public List<Book> retrieveAllBooks() {

		return jdbcTemplate.query(sql.RETRIVE_ALL, (rs, rowNum) -> new Book(rs.getLong("id"), rs.getString("title"),
				rs.getString("author"), rs.getString("genre")));

	}

	/*
	 * Method for searching for all books by an author. returns all books form any
	 * authors that contain the actual argument.
	 */
	@Override
	public List<Book> searchAllByAuthor(String author) {

		return jdbcTemplate.query(sql.RETRIVE_ALL_BY_AUTHOR, new Object[] { "%" + author },
				(rs, rowNum) -> new Book(rs.getLong("id"), rs.getString("title"), rs.getString("author"),
						rs.getString("genre"))

		);
	}

	/*
	 * Method for searching for all books by an genre. returns all books form any
	 * authors that contain the actual argument.
	 */
	@Override
	public List<Book> searchAllByGenre(String genre) {
		return jdbcTemplate.query(sql.RETRIVE_ALL_BY_AUTHOR, new Object[] { "%" + genre },
				(rs, rowNum) -> new Book(rs.getLong("id"), rs.getString("title"), rs.getString("author"),
						rs.getString("genre")));
	}

	/**
	 *Method for searching for all books by an title. returns all books form any
	 * authors that contain the actual argument.
	 */
	@Override
	public List<Book> searchAllByTitle(String title) {
		return jdbcTemplate.query(sql.RETRIVE_ALL_BY_AUTHOR, new Object[] { "%" + title },
				(rs, rowNum) -> new Book(rs.getLong("id"), rs.getString("title"), rs.getString("author"),
						rs.getString("genre")));
	}

	
	
	
	@Override
	public boolean updateBook(Book book) {
		boolean isUpdated = false;
		int intUpdated = jdbcTemplate.update(sql.UPDATE_BOOK, book.getTitle(), book.getAuthor(), book.getGenre(),
				book.getId());
		if (intUpdated == 1)
			isUpdated = true;
		return isUpdated;
	}

	@Override
	public boolean RemoveFromLibrary(long id) {
		boolean isUpdated = false;
		int intUpdated = jdbcTemplate.update(sql.DELETE_SINGLE, id);
		if (intUpdated == 1)
			isUpdated = true;
		return isUpdated;

	}

}
