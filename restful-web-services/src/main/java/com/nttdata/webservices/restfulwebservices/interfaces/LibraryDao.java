package com.nttdata.webservices.restfulwebservices.interfaces;

import java.util.List;

import com.nttdata.webservices.restfulwebservices.model.Book;

public interface LibraryDao {
	
	
	public boolean addBookToLibrary(Book book);
	
	public Book searchById(Long id); 
	
	public List<Book> retrieveAllBooks();
	
	public List<Book> searchAllByAuthor(String author);
	
	public List<Book> searchAllByGenre(String genre);
	
	public List<Book> searchAllByTitle(String Title);
	
	public boolean updateBook(Book book);
	
	public boolean RemoveFromLibrary(long id);
	
	
	
	
	
	
	
	
	
	
}
