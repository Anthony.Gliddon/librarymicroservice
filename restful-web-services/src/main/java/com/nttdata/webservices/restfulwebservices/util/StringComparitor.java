package com.nttdata.webservices.restfulwebservices.util;

import java.util.Comparator;

import com.nttdata.webservices.restfulwebservices.interfaces.Utilities;
import com.nttdata.webservices.restfulwebservices.model.Book;

public class StringComparitor implements Comparator<Book>, Utilities {

	
	public static int compareAuthor(Book b1, Book b2) {
		
		return b1.getAuthor().compareTo(b2.getAuthor());
	}


	public static int compareTitle(Book b1, Book b2) {
		return b1.getTitle().compareTo(b2.getTitle());
	}


	public static int compareGenre(Book b1, Book b2) {
		
		
		return b1.getGenre().compareTo(b2.getGenre()); 
	}


	@Override
	public int compare(Book b1, Book b2) {
		
		return b1.getTitle().compareTo(b2.getTitle());
	}

	
}
