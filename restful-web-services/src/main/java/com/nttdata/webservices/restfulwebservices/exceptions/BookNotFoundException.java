package com.nttdata.webservices.restfulwebservices.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus
public class BookNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -1571971657279938572L;
	private final String exceptionMessage = "Book Not Found";
	
	public BookNotFoundException() {
		super();
		System.out.println(exceptionMessage);
	}
	
}
