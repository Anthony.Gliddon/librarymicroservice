package com.nttdata.webservices.restfulwebservices.interfaces;

import com.nttdata.webservices.restfulwebservices.model.Book;
import com.nttdata.webservices.restfulwebservices.model.CustomResponse;

public interface LibraryServices {
	
	
	public CustomResponse addBookToLibrary(Book newBook);
	
	public CustomResponse searchById(Long id); 
	
	public CustomResponse retrieveAllBooks();
	
	public CustomResponse searchAllByAuthor(String author);
	
	public CustomResponse searchAllByGenre(String genre);
	
	public CustomResponse searchAllByTitle(String Title);
	
	public CustomResponse updateBook(Book book);
	
	public CustomResponse RemoveFromLibrary(long id);

}
